# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Takuya Noguchi <takninnovationresearch@gmail.com>
# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Contributor: Artem Korezin <source-email@yandex.ru>
# Maintainer: Artem Korezin <source-email@yandex.ru>
pkgname=ansible-core
_pkgname=ansible-core
# renovate: datasource=pypi depName=ansible-core
pkgver=2.13.0
pkgrel=99
pkgdesc='A configuration-management, deployment, task-execution, and multinode orchestration framework'
url='https://ansible.com/'
arch='noarch'
license='GPL-3.0-or-later'

# renovate: datasource=repology depName=alpine_3_16/python3 versioning=loose
depends="$depends python3=3.10.4-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-yaml versioning=loose
depends="$depends py3-yaml=6.0-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-paramiko versioning=loose
depends="$depends py3-paramiko=2.11.0-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-jinja2 versioning=loose
depends="$depends py3-jinja2=3.0.3-r1"
# renovate: datasource=repology depName=alpine_3_16/py3-markupsafe versioning=loose
depends="$depends py3-markupsafe=2.1.1-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-cryptography versioning=loose
depends="$depends py3-cryptography=3.4.8-r1"
# renovate: datasource=repology depName=alpine_3_16/py3-resolvelib versioning=loose
depends="$depends py3-resolvelib=0.5.4-r0"

# renovate: datasource=repology depName=alpine_3_16/py3-setuptools versioning=loose
makedepends="$makedepends py3-setuptools=59.4.0-r0"

source="https://files.pythonhosted.org/packages/source/${_pkgname::1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
options='!check'

build() {
	python3 -B setup.py -q build
}

package() {
	python3 -B setup.py -q install --prefix=/usr --root="$pkgdir"
}
sha512sums="
37e17d73477ba88c62af94a6912f9b4b91ebf2ac967c2845875e293fe2ed09307e414644899714e604883973e241eed571d61b7a093a6c212446a1f5b702af6f  ansible-core-2.13.0.tar.gz
"
