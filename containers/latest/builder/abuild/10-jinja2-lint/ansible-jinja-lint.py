#!/usr/bin/env python3
# pylint: disable-msg=invalid-name
"""
Ansible jinja2 linter main exec.

@author Gerard van Helden <drm@melp.nl>
@license DBAD, see <http://www.dbad-license.org/>
"""
import jinja2
from j2lint import main, AbsolutePathLoader

filters = [
    "regex_replace",
    "quote",
    "password_hash",
    "to_yaml",
    "to_nice_yaml",
    "match",
    "to_json",
    "to_nice_json",
    "combine",
]
tests = [
    "match",
]
env = jinja2.Environment(loader=AbsolutePathLoader())  # nosec: B701
env.filters.update({name: lambda: None for name in filters})
env.tests.update({name: lambda: None for name in tests})

main(env=env)
